#!/bin/bash

# convert static library to shared
# Latyshev Grigory

stlib="$1"
shlibpath="$(dirname $stlib)"
shlibpath=$(cd $shlibpath; pwd)
shlibname="$(basename $(echo $stlib | sed 's,\.a$,.so,g'))"

tmpdir=$(mktemp -d)
cp $stlib $tmpdir/
cd $tmpdir
stlib="$(basename $stlib)"

ar -x $stlib
ld --shared -o res.so *.o && cp res.so $shlibpath/$shlibname

cd - 1>/dev/null
rm -rf $tmpdir
