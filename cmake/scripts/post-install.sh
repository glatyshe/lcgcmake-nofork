#!/bin/sh

export LANG=C
listname=".filelist"

# === Replace install paths in files from filelist
THIS="$0"
PREFIX="$(/bin/sh -c "dirname $THIS")"

while [ ! -d "$INSTALLDIR" ]; do
  [ ! -z "$RPM_INSTALL_PREFIX" ] && INSTALLDIR="$RPM_INSTALL_PREFIX" || read -e -p "Type new install directory : " INSTALLDIR
done

OLDINSTALLDIR="$(head -1 $PREFIX/$listname)"
sed -i -e '1d' $PREFIX/$listname
echo 
echo "Replacing $OLDINSTALLDIR -> $INSTALLDIR"
echo

# prepare package list for replacing

cat $PREFIX/$listname | grep -v -- '->' | while read name; do
  echo "=== Patching $name ... ==="
  old=$(mktemp -t lcg.XXXXX)
  cp -f "$PREFIX/$name" "$old"
  # patch files
  cat $PREFIX/$listname | grep -- '->' | while read line; do
    olddir="$(echo $line | awk -F'->' '{print $1}')"
    newdir="${INSTALLDIR}/$(echo $line | awk -F'->' '{print $2}')"   
    test ! -z "$olddir" && sed -i -e "s@$olddir@$newdir@g" -e 's,\.cern.ch,cern.ch,g' "$PREFIX/$name"
  done 
  diff -u "$old" "$PREFIX/$name"
  rm -f "$old"
  echo
done

exit 0
